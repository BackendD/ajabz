package ir.backendd.ajab.service

import ir.backendd.ajab.domain.User
import ir.backendd.ajab.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserSecurityService : UserDetailsService {

    @Autowired
    private lateinit var userRepository: UserRepository


    override fun loadUserByUsername(username: String): UserDetails {
        var user: User? = userRepository.findByUsername(username)
        if(null == user) {
            log.warn("Username {} not found", username);
            throw UsernameNotFoundException("Username "+username+" not found");
        }
        return user
    }
}

val log: Logger = LoggerFactory.getLogger(UserSecurityService::class.java)