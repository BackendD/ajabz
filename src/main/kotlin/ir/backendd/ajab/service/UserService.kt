package ir.backendd.ajab.service

import ir.backendd.ajab.domain.Role
import ir.backendd.ajab.domain.User

interface UserService {
    fun findAll(): Set<User>
    fun createUser(user: User): User
    fun findByUsername(username: String): User?
    fun save(user: User): User
    fun removeOneById(id: Long)
}