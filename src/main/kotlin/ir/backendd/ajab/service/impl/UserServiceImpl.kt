package ir.backendd.ajab.service.impl

import ir.backendd.ajab.domain.Role
import ir.backendd.ajab.domain.User
import ir.backendd.ajab.repository.RoleRepository
import ir.backendd.ajab.repository.UserRepository
import ir.backendd.ajab.service.UserService
import org.slf4j.LoggerFactory
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl : UserService {

	@Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var roleRepository: RoleRepository

    override fun createUser(user: User): User {
        var localUser: User? = findByUsername(user.username)

        if(localUser != null)
            log.info("User with username {} already exist. Nothing will be done. ", user.username)
        else
            localUser = userRepository.save(user)
        return localUser
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun findByUsername(username: String): User? {
        return userRepository.findByUsername(username)
    }

    override fun findAll(): Set<User> {
        var users: Set<User> = userRepository.findAll().toHashSet()
        return users
    }

    override fun removeOneById(id: Long) {
        userRepository.deleteById(id)
    }

}

val log: Logger = LoggerFactory.getLogger(UserService::class.java)