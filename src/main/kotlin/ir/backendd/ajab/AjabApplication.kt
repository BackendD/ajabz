package ir.backendd.ajab

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AjabApplication

fun main(args: Array<String>) {
	runApplication<AjabApplication>(*args)
}
