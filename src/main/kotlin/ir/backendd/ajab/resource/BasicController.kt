package ir.backendd.ajab.resource

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class BasicController {

    @GetMapping("/", "/home", "index")
    fun home(): String = "home"

    @GetMapping("/login")
    fun login(): String = "login"
}