package ir.backendd.ajab.resource

import ir.backendd.ajab.config.passwordEncoder
import ir.backendd.ajab.domain.Role
import ir.backendd.ajab.service.UserService
import ir.backendd.ajab.domain.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController("/user")
class UserResource {

    @Autowired
    lateinit var userService: UserService;

    @PostMapping
    fun registration(@RequestBody mapper: HashMap<String, String>): ResponseEntity<String> {

        var username: String = mapper.get("username")?: ""

        if (userService.findByUsername(username) != null) {
            return ResponseEntity("username Exists", HttpStatus.BAD_REQUEST);
		}

        var user = User()

        user.username = username
        user.password = passwordEncoder().encode(user.password)
        user.firstName = mapper.get("firstName")?: ""
        user.lastName = mapper.get("lastName")?: ""

        var role = Role()
        role.name = "customer"
        user.roles.add(role)

        userService.save(user)

        return ResponseEntity("User Added Successfully!", HttpStatus.OK);
    }
}