package ir.backendd.ajab.config

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.security.SecureRandom

class SecurityUtility

val SALT: String = "boro_baba"

fun passwordEncoder(): BCryptPasswordEncoder {
    return BCryptPasswordEncoder(12, SecureRandom(SALT.toByteArray()))
}