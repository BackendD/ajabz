package ir.backendd.ajab.config

import ir.backendd.ajab.service.UserSecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var userSecurityService: UserSecurityService

    override protected fun configure(http: HttpSecurity) {
        http
            .authorizeRequests()
                .antMatchers("/", "/home", "/index").permitAll()
                .antMatchers(HttpMethod.POST,"/user").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll()
                .and()
            .csrf().disable()
    }

    @Autowired
	fun configureGlobal(auth: AuthenticationManagerBuilder) {
		auth.userDetailsService(userSecurityService).passwordEncoder(passwordEncoder());
	}
}
