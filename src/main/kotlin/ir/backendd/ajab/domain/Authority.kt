package ir.backendd.ajab.domain

import org.springframework.security.core.GrantedAuthority

class Authority (name: String) : GrantedAuthority {
    val _name: String = name
    override fun getAuthority(): String {
        return _name
    }
}