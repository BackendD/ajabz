package ir.backendd.ajab.domain

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import java.util.HashSet



@Entity
@Table(name = "users")
class User : UserDetails {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	var id: Long = 0
	var firstName: String = ""
	var lastName: String = ""
	private var username: String = ""
	private var password: String = ""
	@ManyToMany(cascade=[CascadeType.ALL], fetch = FetchType.EAGER)
	@JoinTable(name = "users_roles")
	var roles: MutableSet<Role> = HashSet<Role>()

	override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
		var authorities: MutableSet<GrantedAuthority>  = HashSet<GrantedAuthority>()
		roles.forEach{r -> authorities.add(Authority(r.name))}
		return authorities
	}

	override fun getUsername(): String = username
	fun setUsername(username: String) {
		this.username = username
	}
	override fun getPassword(): String = password
	fun setPassword(password: String) {
		this.password = password
	}
	override fun isAccountNonExpired(): Boolean = true
	override fun isAccountNonLocked(): Boolean = true
	override fun isCredentialsNonExpired(): Boolean = true
	override fun isEnabled(): Boolean = true
}