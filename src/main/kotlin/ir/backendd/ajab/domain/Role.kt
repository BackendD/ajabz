package ir.backendd.ajab.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
class Role {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    var name: String = ""
    @ManyToMany(mappedBy = "roles")
    var users: MutableSet<User> = HashSet<User>()
}