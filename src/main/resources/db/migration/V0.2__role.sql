CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(255)
);
ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);