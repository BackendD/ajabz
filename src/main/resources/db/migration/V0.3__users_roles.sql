CREATE TABLE public.users_roles (
    users_id bigint NOT NULL,
    roles_id bigint NOT NULL
);
ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (users_id, roles_id);
ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT rkey FOREIGN KEY (roles_id) REFERENCES public.role(id);
ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT ukey FOREIGN KEY (users_id) REFERENCES public.users(id);
