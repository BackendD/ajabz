FROM openjdk:11
ADD /build/libs/*.jar app.jar
ADD /application.yml application.yml
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]